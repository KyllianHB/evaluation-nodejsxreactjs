const mysql = require('mysql2');

// Configuration de la connexion à la base de données MySQL
const connection = mysql.createPool({
  host: `${process.env.DB_HOSTNAME}`,
  port: `${process.env.DB_PORT}`,
  user: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  database: `${process.env.DB_DATABASE}`
}).promise()

// Vérification de la connexion à la base de données MySQL
connection.getConnection()
  .then((conn) => {
    console.log('Connecté à la base de données ' + process.env.DB_DATABASE + ' avec l\'identifiant ' + conn.threadId);
    conn.release();
  })
  .catch((err) => {
    console.error('Erreur de connexion à la base de données MySQL : ' + err.stack);
  });

module.exports = connection;
