const jwt = require('jsonwebtoken')
const connection = require('../data/helpers/db')

const loginUser = async (credentials, callback) => {
  let _error

  if (!credentials.data.email || !credentials.data.mdp) {
    _error = 'Invalid Credentials'
  }
  try {
    var query = `SELECT user_id, user_email, user_motdepasse FROM utilisateur WHERE user_motdepasse like "${credentials.data.mdp}" AND user_email like "${credentials.data.email}"`
    const [user, fields] = await connection.query(query);
  
    if (!user) {
      _error = 'Invalid Credentials'
      return callback(_error, null)
    } else {
      const payload = {
        id: user[0].user_id,
        email: user[0].user_email
      }

      jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: '7d' }, (error, token) => {
        if (error) {
          _error = 'Invalid Credentials'
        }

        return callback(error, {
          user,
          token
        })
      })
    }
  } catch (error) {
    console.log(query)
    console.log(error)
    throw error
  }

}

module.exports = {
  loginUser
}
