const { query } = require('express');
const connection = require('../data/helpers/db')

const addUser = async (user) => {

    try {
        var query = `INSERT INTO utilisateur (user_nom, user_prenom, user_telephone, user_email, user_motdepasse, user_photo) VALUES ("${user.nom}","${user.prenom}","${user.telephone}","${user.email}","${user.mdp}","${user.photo}");`
        const [rows, fields] = await connection.query(query);
        // console.log(rows)
        return {user, rows};
    } catch (error) {
        console.error(error);
        throw error;
    }
    
}

const updateUser = async (user) => {

    try {
        var query = `UPDATE utilisateur SET user_nom = "${user.nom}", user_prenom = "${user.prenom}", user_telephone = "${user.telephone}", user_email = "${user.email}", user_motdepasse = "${user.mdp}", user_photo = "${user.photo}" WHERE user_id = ${user.id}`
        const [rows, fields] = await connection.query(query);
        return {user, rows};
    } catch (error) {
        console.error(error);
        throw error;
    }
    
}

const getUser = async (id) => {
    try {
        if(id) {
            const [rows, fields] = await connection.query(`SELECT * FROM utilisateur WHERE user_id = ${parseInt([id])}`);
            return rows
        } else {
            const [rows, fields] = await connection.query('SELECT * FROM utilisateur');
            return rows;
        }
    } catch (error) {
        console.error(error);
        throw error;
    }
}

const removeUser = async (id) => {

    try {
        var query = `DELETE FROM utilisateur WHERE user_id = ${parseInt([id])}`
        const [rows, fields] = await connection.query(query);
        return rows;
    } catch (error) {
        console.error(error);
        throw error;
    }
    
}

module.exports = {
    addUser,
    getUser,
    updateUser,
    removeUser
  }