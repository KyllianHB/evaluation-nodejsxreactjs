const connection = require('../data/helpers/db')

const getFoo = async () => {
    
    try {
        const [rows, fields] = await connection.query('SELECT * FROM planchesurf');
        return rows;
    } catch (error) {
        console.error(error);
        throw error;
    }
    
}

module.exports = {
    getFoo
  }