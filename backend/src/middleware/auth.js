const jwt = require('jsonwebtoken')

const withAuth = (req, res, next) => {

  console.log(req.headers)

  if (req.headers.authorization) {
    try {
      const decoded = jwt.verify(req.headers.authorization.split(' ')[1], process.env.TOKEN_SECRET)

      console.log(decoded)
      console.log(req.params)

      //Verfication que l'id en parametre corresponde à l'id présent dans le token
      if (decoded && decoded.id == req.params.id) {
        req.userId = decoded.id
        next()
      } else {
        return res.status(401).send()
      }
    } catch (error) {
      return res.status(401).send()
    }
  } else {
    return res.status(401).send()
  }
}

module.exports = withAuth
