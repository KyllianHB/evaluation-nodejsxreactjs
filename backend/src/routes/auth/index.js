const router = require('express').Router()
const { loginUser } = require('../../controllers/authController.js')

router.route('/login').post(async (req, res) => {
    const credentials = req.body

    try {
      loginUser(credentials, (error, result) => {
        return res.send(result)
      })
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

module.exports = router
