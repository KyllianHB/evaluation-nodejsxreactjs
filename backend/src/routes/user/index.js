const { response } = require('express')
const { addUser, getUser, updateUser, removeUser } = require('../../controllers/userController')
const withAuth = require('../../middleware/auth')

// const withAuth = require('../../middlewares/auth')
const router = require('express').Router()

// --- CREATE ---

// Ajouter un utilisateur
router.route('/registration').post(async (req, res) => {

    try {
        const result = await addUser(req.body.data)
        return res.status(201).send(result) 

    } catch (error) {
        return res.status(500).send(error)
    }
})

// --- READ ---

// Afficher les utilisateurs
router.route('/').get(async (req, res) => {
    try {
        const result = await getUser()
        res.status(200).send(result)
    } catch (error) {
        return res.status(500).send(error)
    } 
})


// Afficher un utilisateur
router.route('/:id').get(withAuth, async (req, res) => {
    try {
        const result = await getUser(`${[req.params.id]}`)
        res.status(200).send(result)
    } catch (error) {
        return res.status(500).send(error)
    }
})


// --- UPDATE ---

// Modifier un utilisateur
router.route('/update/:id').post(withAuth, async (req, res) => {

    try {
        const result = await updateUser(req.body.data)
        return res.status(201).send(result) 
        
    } catch (error) {
        return res.status(500).send(error)
    }
})

// --- DELETE ---

// Supprimer un utilisateur
router.route('/remove/:id').delete(withAuth, async (req, res) => {

    try {
        const result = await removeUser(`${[req.params.id]}`)
        return res.status(202).send(result) 
        
    } catch (error) {
        return res.status(500).send(error)
    }
})

  module.exports = router