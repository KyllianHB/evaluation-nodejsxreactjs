const { getFoo } = require('../../controllers/fooController')

// const withAuth = require('../../middlewares/auth')
const router = require('express').Router()

router.route('/')
// Récupérer la liste des users
    .get(async (req, res) => {
        try {
            const listFoo = await getFoo()
            return res.status(200).send(listFoo)
        } catch (error) {
            return res.status(500).send(error)
        }
    })

router.route('/bar')
// Récupérer la liste des users
    .get(async (req, res) => {
        try {
            return res.status(200).send('omg bar')
        } catch (error) {
            return res.status(500).send(error)
        }
    })

  module.exports = router