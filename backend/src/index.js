// --- Appel des variables d'environnement ---
require('dotenv').config()

// --- Initialisation du serveur ---
const bodyParser = require('body-parser');
const express = require('express');
const app = express()
const port = `${process.env.APP_PORT}`
const server = app.listen(port, () => {
  console.log('Serveur OK | port ' + port)
})

// --- Réglages pour l'envoie de fichier ---
// Limiter la taille du payload à 2mo
app.use(bodyParser.json({ limit: '2mb' }));
app.use(bodyParser.urlencoded({ limit: '2mb', extended: true }));

// Augmentation du temps du timeout
server.timeout = 30000; // 30 secondes

// --- Gestion du CORS ---
const cors = require('cors')
app.use(cors())

// --- Paramètrage d'Express ---
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// --- Logger ---
const morgan = require('morgan')
app.use(morgan('combined'))

// --- Routes ---
// On branche nos routes sur le fichier correspondant, le nom index.js est utilisé par défaut.
app.use('/foo', require('./routes/foo'))
app.use('/user', require('./routes/user'))
app.use('/auth', require('./routes/auth'))


