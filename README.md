# Lancement du projet

  

### Première installation :

1. Se mettre à la racine du projet

2. Ecrire dans la console : `yarn launch` ou `npm launch`

  

> => Les packages s'installent et le frontend et le backend démarrent

  

  

### Si les packages sont déjà installés :

1. Se mettre à la racine du projet

2. Ecrire dans la console : `yarn start` ou `npm start`

  

> => Le frontend et le backend démarrent

  

### Les fonctionnalités :

Il est possible de :
 - S'inscrire
 - Se connecter / Déconnecter
 - Voir sa page profil
 - Modifier son profil 
 - Ajouter une photo

Sécurité : 
 - Les routes sont protégées par un middleware
 - Le JWT est utilisé et stocké dans le "local storage" au moment de l'authentification et de l'inscription