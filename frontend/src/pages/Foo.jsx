import { useState, useEffect } from 'react'
import { getFoo } from '../services/Api'

function Foo () {
  const [foo, setFoo] = useState()

  useEffect(() => {
    const getData = async () => {
      const result = await getFoo()
      setFoo(result)
    }
    getData()
  }, [])

  if (!foo) {
    return <h1>Chargement...</h1>
  }

  console.log(foo)
  return (
    <>
      <h1>FOO !</h1>
      
      {foo.map(obj => (
        <div key={obj.psurf_id}>
          <p>{obj.psurf_nom}</p>
          <p>{obj.psurf_taille}</p>
        </div>
      ))}
      
    </>
  )
}

export default Foo
