import React, { useState } from "react";
import { addUser, auth } from "../services/Api";
import  { useNavigate } from 'react-router'
import Lottie from 'react-lottie';
import animationData from '../lotties/loader';
import { useAuth } from "../contexts/AuthContext";


function Inscription() {

  const [formData, setFormData] = useState({
    photo: '',
    nom: 'Foo',
    prenom: 'Bar',
    telephone: '0606060606',
    email: 'foobar@gmail.fr',
    mdp: 'foobar'
  })

  const [inscriptionReussie, setInscriptionSucces] = useState(false);
  const [viewLoader, setViewLoader] = useState(false);
  const {login} = useAuth()
  const redirection = useNavigate();

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  // Convertit l'image transférée en format base64
  const handleImage = (event) => {
    const file = event.target.files[0]
    const reader = new FileReader();
  
    if (file) {
      // Vérification de l'extension du fichier
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        // Vérification la taille du fichier
        if (file.size <= 400 * 1024) { // 400 Ko
          /* Le fichier est passé en tant qu'argument à la méthode readAsDataURL, 
          ce qui déclenche la lecture du fichier. 
          Une fois que la lecture est terminée avec succès, 
          l'événement load est déclenché et la fonction de rappel reader.onload est exécutée. */
          reader.readAsDataURL(file);
        } else {
          alert('La taille de l\'image ne doit pas dépasser 400 Ko.');
        }
      } else {
        alert('Veuillez sélectionner une image au format JPG ou PNG.');
      }
    }

    reader.onload = () => {
      const dataUrl = reader.result
      setFormData({
        ...formData,
        [event.target.name]: dataUrl
      })
    }


  }

  const handleSubmit = async (event) => {
    
    event.preventDefault();
    if (formData.nom && formData.prenom && formData.telephone && formData.email && formData.mdp) {
      
      setViewLoader(true);
      const result = await addUser(formData)

      if(result.rows.insertId){

        const result = await auth(formData)

        if(result.user[0]) {
          setViewLoader(true);
          setInscriptionSucces(true)

          result.token = localStorage.setItem('auth_glidebox', result.token);

          setTimeout(() => {
            redirection(`/user/${result.user[0].user_id}`);
            login()
          }, 1000);
        }
      }

    } else {
      alert("remplir les champs")
    }
  };

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };

  return (
    <div>
      <h2>Inscription</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Photo: (max 400 ko)
          <input
            type="file"
            name="photo"
            accept="image/jpeg, image/png"
            onChange={handleImage}
          />
        </label>
        <br/> 
        <label>
          Nom:
          <input
            type="text"
            name="nom"
            value={formData.nom}
            onChange={handleChange}
          />
        </label>
        <br />
        <label>
          Prénom:
          <input
            type="text"
            name="prenom"
            value={formData.prenom}
            onChange={handleChange}
          />
        </label>
        <br />
        <label>
          Téléphone:
          <input
            type="tel"
            name="telephone"
            value={formData.telephone}
            onChange={handleChange}
          />
        </label>
        <br />
        <label>
          Email:
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
          />
        </label>
        <br />
        <label>
          Mot de passe:
          <input
            type="password"
            name="mdp"
            onChange={handleChange}
          />
        </label>
        <br />
        {
          viewLoader ? (
              <Lottie 
                options={defaultOptions}
                height={40}
                width={40}
              />
          ) : <button type="submit">S'inscrire</button>
        }
      </form>
      {
        inscriptionReussie && (
          <>
            <p className="success_message">Inscription réussie</p>
          </> 
        )
        
      }
    </div>
  );
}

export default Inscription;
