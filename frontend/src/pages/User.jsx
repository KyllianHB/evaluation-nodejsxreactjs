import React, { useState, useEffect } from 'react';
import { getUserById, removeUserById, updateUserById } from '../services/Api'; // Assumant que tu as une fonction updateUserById pour mettre à jour les informations de l'utilisateur
import Lottie from 'react-lottie';
import animationData from '../lotties/loader';
import { useNavigate, useParams } from 'react-router-dom';

function User() {
  const [user, setUser] = useState();
  const [formData, setFormData] = useState({
    photo: '',
    email: '',
    password: '',
    nom: '',
    prenom: '',
    telephone: ''
  });
  const [isEditing, setIsEditing] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [viewLoader, setViewLoader] = useState(false);

  const urlParams = useParams();
  const redirection = useNavigate();


  useEffect(() => {
    const getData = async () => {
      const result = await getUserById(urlParams.id);

      setUser(result[0]);
      setFormData({
        photo: result[0]?.user_photo || '',
        email: result[0]?.user_email || '',
        mdp: result[0]?.user_motdepasse || '',
        nom: result[0]?.user_nom || '',
        prenom: result[0]?.user_prenom || '',
        telephone: result[0]?.user_telephone || ''
      });
    };
    getData();
  }, [urlParams.id]);

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const handleClick = async () => {
    const result = await removeUserById(urlParams.id)
    if(result.affectedRows === 1){
      setViewLoader(true)
      const _data = {
        deleteUserSuccess: 1
      }
      setTimeout(() => {
        redirection(`/`, _data);
      }, 1000);
    }
  }

  const handleSubmit = async (event) => {
    setViewLoader(true)
    event.preventDefault();
    // Appel à la fonction pour mettre à jour les informations de l'utilisateur
    const result = await updateUserById(urlParams.id, formData);

    if(result.rows.affectedRows >= 1){
      setUser({
        user_photo: result.user.photo,
        user_nom: result.user.nom,
        user_prenom: result.user.prenom,
        user_email: result.user.email,
        user_motdepasse: result.user.mdp,
        user_telephone: result.user.telephone
      });

      setTimeout(() => {
        setViewLoader(false)
        setIsUpdate(true) 
        setIsEditing(false);
      }, 1000);
    }

  };

  const handleImage = (event) => {
    const file = event.target.files[0]
    const reader = new FileReader();
  
    if (file) {
      // Vérification de l'extension du fichier
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        // Vérification la taille du fichier
        if (file.size <= 400 * 1024) { // 400 Ko
          /* Le fichier est passé en tant qu'argument à la méthode readAsDataURL, 
          ce qui déclenche la lecture du fichier. 
          Une fois que la lecture est terminée avec succès, 
          l'événement load est déclenché et la fonction de rappel reader.onload est exécutée. */
          reader.readAsDataURL(file);
        } else {
          alert('La taille de l\'image ne doit pas dépasser 400 Ko.');
        }
      } else {
        alert('Veuillez sélectionner une image au format JPG ou PNG.');
      }
    }

    reader.onload = () => {
      const dataUrl = reader.result
      setFormData({
        ...formData,
        [event.target.name]: dataUrl
      })
    }


  }

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  if (!user) {
    return (
      <Lottie options={defaultOptions} height={100} width={100} />
    );
  }

  return (
    <>
      {!isEditing ? (
        <>
          <h1>Bienvenue {user.user_nom}</h1>
          <div>
            <img width={100} src={user.user_photo} />
            <p>Email: {user.user_email}</p>
            <p>Mot de passe: {user.user_motdepasse}</p>
            <p>Nom: {user.user_nom}</p>
            <p>Prénom: {user.user_prenom}</p>
            <p>Téléphone: {user.user_telephone}</p>
            <button onClick={() => setIsEditing(true)}>Modifier</button>
            {
              isUpdate && (
                <>
                  <p className="success_message">Modifications réussies</p>
                </> 
              ) 
            }
          </div>
        </>
      ) : (
        <form onSubmit={handleSubmit}>
          <h1>Modifier les informations</h1>
          <div>
            <label>
              Photo: (max 400 ko)
              <input
                type="file"
                name="photo"
                accept="image/jpeg, image/png"
                onChange={handleImage}
              />
            </label>
            <br />
            <label>Email:</label>
            <input
              type="email"
              name="email"
              value={formData.email}
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Mot de passe:</label>
            <input
              type="password"
              name="mdp"
              value={formData.mdp}
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Nom:</label>
            <input
              type="text"
              name="nom"
              value={formData.nom}
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Prénom:</label>
            <input
              type="text"
              name="prenom"
              value={formData.prenom}
              onChange={handleChange}
            />
          </div>
          <div>
            <label>Téléphone:</label>
          <input
           type="text"
           name="telephone"
           value={formData.telephone}
           onChange={handleChange}
          />
          </div>
          {viewLoader ? (
                <Lottie 
                  options={defaultOptions}
                  height={40}
                  width={40}
                />
            ) : <>
                  <button type="submit">Enregistrer</button>
                  <button type="button" onClick={() => setIsEditing(false)}>Annuler</button>
                  <button type="button" onClick={handleClick}>Supprimer</button>
                </>
          }
        </form>
      )}
    </>
)}
export default User;