import React, { useState } from "react";
import { auth } from "../services/Api";
import  { useNavigate } from 'react-router'
import Lottie from "react-lottie";
import animationData from '../lotties/loader';


function Connexion() {
  const [formData, setFormData] = useState({
    email: 'foobar@gmail.fr',
    mdp: 'foobar'
  })

  const redirection = useNavigate();
  const [viewLoader, setViewLoader] = useState(false);

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = async (event) => {
    
    event.preventDefault();
    if (formData.email && formData.mdp) {
      
      // setViewLoader(true);
      const result = await auth(formData)

      if(result.user[0]) {
        setViewLoader(true);
        localStorage.setItem('auth_glidebox', result.token);
        localStorage.setItem('user_glidebox', result.user[0].user_id);
        setTimeout(() => {
          redirection(`/user/${result.user[0].user_id}`);
          window.location.reload()
        }, 1000);
      }

    } else {
      alert("remplir les champs")
    }
  };

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };

  return (
    <div>
      <h2>Connexion</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Email:
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
          />
        </label>
        <br />
        <label>
          Mot de passe:
          <input
            type="password"
            name="mdp"
            value={formData.mdp}
            onChange={handleChange}
          />
        </label>
        <br />
        {
          viewLoader ? (
              <Lottie 
                options={defaultOptions}
                height={40}
                width={40}
              />
          ) : <button type="submit">Se connecter</button>
        }
      </form>
    </div>
  );
}

export default Connexion;
