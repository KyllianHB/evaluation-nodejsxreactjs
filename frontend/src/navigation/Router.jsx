import { Route, Routes } from 'react-router-dom'
import Foo from '../pages/Foo'
import Connexion from '../pages/Connexion'
import Inscription from '../pages/Inscription'
import User from '../pages/User'

function Router () {
  return (
    <Routes>
      <Route index path='/' element={<Foo />} />
      <Route index path='/connexion' element={<Connexion />} />
      <Route index path='/inscription' element={<Inscription />} />
      <Route index path='/user/:id' element={<User />} />
    </Routes>
  )
}

export default Router
