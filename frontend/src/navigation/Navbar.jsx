import { useAuth } from "../contexts/AuthContext";

function Navbar () {
  
	const { state: { isAuthenticated } } = useAuth()

	console.log(isAuthenticated)

	function handleClick() {
		localStorage.removeItem('auth_glidebox');
		localStorage.removeItem('authContext');
	}
	
return (
	<nav>
		<a href="/">Accueil</a>
		<div className="right">
			{
				isAuthenticated ? 
					(<>
						<a href={`/user/${localStorage.getItem('user_glidebox')}`}>Profil</a>
						<a href="/" onClick={handleClick}>Deconnexion</a>
					</>)
				:
					(<>
						<a href="/inscription">Inscription</a>
						<a href="/connexion">Connexion</a>
					</>)
			}
		</div>
	</nav>
  )
}

export default Navbar
