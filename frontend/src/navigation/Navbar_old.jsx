import { useEffect, useState } from "react";
import { redirect, useNavigate } from "react-router-dom";

function Navbar () {
  
	const [isLoggedIn, setIsLoggedIn] = useState(false)
	const redirection = useNavigate()

	useEffect(() => {
		setIsLoggedIn(!!localStorage.getItem('auth_glidebox'));
	}, [])

	console.log(localStorage.getItem('user_glidebox'))

	function handleClick() {
		localStorage.removeItem('auth_glidebox');
		setIsLoggedIn(false);
	  }
	
return (
	<nav>
		<a href="/">Accueil</a>
		<div className="right">
			{
				isLoggedIn ? 
					(<>
						<a href={`/user/${localStorage.getItem('user_glidebox')}`}>Profil</a>
						<a onClick={handleClick}>Deconnexion</a>
					</>)
				:
					(<>
						<a href="/inscription">Inscription</a>
						<a href="/connexion">Connexion</a>
					</>)
			}
		</div>
	</nav>
  )
}

export default Navbar
