import axios from 'axios'

const api = axios.create({
  baseURL: 'http://localhost:4000/',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  },
  timeout: 30000
})

//Permet de renvoyer le header pour le mettre en parametre dans les appels d'api
const _headers = () => {
  return (
  {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('auth_glidebox')}`,
    },
  }
  )
}


const getFoo = async () => {
    try {
      const response = await api.get('/foo')
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

  //--- CREATE ---

  // Appel d'api pour ajouter un utilisateur dans la bdd
  const addUser = async (formData) => {
    try {
      const _data = {
        data: { 
          photo: formData.photo,
          nom: formData.nom,
          prenom: formData.prenom,
          telephone: formData.telephone,
          email: formData.email,
          mdp: formData.mdp
        }
      }
      const response = await api.post('/user/registration', _data)
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

// --- READ ---
  const getUser = async (id) => {
    try {
        const response = await api.get('/user')
        return response.data
    } catch (error) {
      console.error(error)
    }
  }

  // Appel d'api pour recuperer les informations d'un utilisateur en fonction de son id
  const getUserById = async (id) => {
    try {
        const response = await api.get(`/user/${id}`, _headers())
        return response.data
    } catch (error) {
      console.error(error)
    }
  }

// --- UPDATE ---

  // Appel d'api pour modifier les informations d'un utilisateur en fonction de son id
  const updateUserById = async (id, formData) => {
    try {
      const _data = {
        data: { 
          id: id,
          photo: formData.photo,
          nom: formData.nom,
          prenom: formData.prenom,
          telephone: formData.telephone,
          email: formData.email,
          mdp: formData.mdp
        }
      }
      const response = await api.post(`/user/update/${id}`, _data, _headers())
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

  // --- DELETE

  // Appel d'api pour supprimer un utilisateur en fonction de son id
  const removeUserById = async (id) => {
    try {

      const _data = {
        data: { 
          id: id,
        }
      }
      console.log(id)

      const response = await api.delete(`/user/remove/${id}`, _data, _headers())
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

  /// --- Authentification ---

  // Appel d'api pour créer le token d'authentification
  const auth = async (formData) => {
    try {
      const _data = {
        data: { 
          email: formData.email,
          mdp: formData.mdp
        }
      }
      const response = await api.post('/auth/login', _data)
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

export {
    getFoo,
    getUser,
    addUser,
    getUserById,
    updateUserById,
    removeUserById,
    auth
}