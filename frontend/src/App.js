import { AuthProvider } from "./contexts/AuthContext";
import Navbar from "./navigation/Navbar";
import Router from "./navigation/Router";

function App() {
  return (
    <div className="App">
      <AuthProvider>
        <Navbar />
        <Router />
      </AuthProvider>
    </div>
  );
}

export default App;
